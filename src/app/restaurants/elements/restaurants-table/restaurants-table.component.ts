import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConstants} from '../../../app.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import {Observable} from 'rxjs/Rx';
import {RestaurantModel} from '../../restaurant-model';

@Component({
  selector: 'app-restaurants-table',
  templateUrl: './restaurants-table.component.html',
  styleUrls: ['./restaurants-table.component.scss']
})
export class RestaurantsTableComponent implements OnInit {
  action: string;
  baseUrl: string;
  public restaurants = [];
  constructor( private client: HttpClient, private spinner: NgxSpinnerService) {

    this.action = 'restaurants';
    this.baseUrl = AppConstants.baseURL ;
}

  ngOnInit() {
  this.spinner.show();
  const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
  this.getRestaurants().subscribe(data  => this.restaurants = data) ;
  this.spinner.hide();

  }

  getRestaurants(): Observable<RestaurantModel[]> {
    return this.client.get<RestaurantModel[]>(this.baseUrl + this.action)
  }
}
