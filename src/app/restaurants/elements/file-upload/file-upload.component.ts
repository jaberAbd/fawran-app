import { Component, OnInit, Output, Input, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType} from '@angular/common/http';
import {AppConstants} from '../../../app.constants';
@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit, OnChanges{
  selectedFile: File;
  action: string;
  baseUrl: string;
  uploadType: string;
  imageLoaded: boolean;
  imageSrc: any;
  imageUploadedId: number ;
  imageSrcText: string;
  @Output() messageEvent = new EventEmitter<string>();
  @Input() imageFromParent: string;
  constructor( private http: HttpClient) {
    this.action = 'restaurant/upload';
    this.baseUrl = AppConstants.baseURL ;
    this.uploadType = 'restaurant';
   }

  ngOnInit() {
    this.imageLoaded = false ;
    this.imageSrc = 'https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX32640113.jpg';
  }



  onFileChanged(event) {
    this.selectedFile = <File> event.target.files[0];
    this.imageLoaded = true  ;
    const reader = new FileReader();
    const binaryReader = new FileReader();
    reader.onload = (event: ProgressEvent) => {
      this.imageSrc = (<FileReader>event.target).result;
    }

    binaryReader.onload = (event: ProgressEvent) => {
      const binaryString = (<FileReader>event.target).result;
      this.messageEvent.emit(btoa(binaryString.toString()));
    }
    reader.readAsDataURL(event.target.files[0]);
    binaryReader.readAsBinaryString(event.target.files[0]);
  }
  onUpload() {
    const uploadData = new FormData();
    uploadData.append('file' , this.selectedFile , this.selectedFile.name);
    uploadData.append('type' , this.uploadType ) ;
    this.http.post(this.baseUrl + this.action, uploadData, {
      reportProgress: true,
      observe: 'events'
    })
      .subscribe(event => {
         if (event.type === HttpEventType.UploadProgress) {
            //  console.log('upload progress ' + Math.round(event.loaded / event.total) * 100 + '%')
         }
         if (event.type === HttpEventType.Response) {

            this.imageUploadedId = event.body['id'];
            // console.log(this.imageUploadedId) ;
         }
 // handle event here
      });
  }



  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    const c = changes['imageFromParent'].currentValue;
    this.imageSrc = 'data:image/jpg;base64,' +  c;
  }



}
