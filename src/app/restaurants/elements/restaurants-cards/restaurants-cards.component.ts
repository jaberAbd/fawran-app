import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConstants} from '../../../app.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import {Observable} from 'rxjs/Rx';
import {RestaurantModel} from '../../restaurant-model';
import { DomSanitizer } from '@angular/platform-browser';
import { element } from '@angular/core/src/render3';
@Component({
  selector: 'app-restaurants-cards',
  templateUrl: './restaurants-cards.component.html',
  styleUrls: ['./restaurants-cards.component.scss']
})
export class RestaurantsCardsComponent implements OnInit {

  action: string;
  baseUrl: string;
  public restaurants = [];
  constructor( private client: HttpClient, private domSanitizer: DomSanitizer, private spinner: NgxSpinnerService) {

    this.action = 'restaurants';
    this.baseUrl = AppConstants.baseURL ;
}

  ngOnInit() {
    this.spinner.show();
    const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
    this.getRestaurants().subscribe(data  => this.restaurants = data) ;

    this.spinner.hide();
}
    getRestaurants(): Observable<RestaurantModel[]> {
      return this.client.get<RestaurantModel[]>(this.baseUrl + this.action)
    }

    decode(encodedImage) {
      return this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
      + encodedImage);
    }

}
