import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantsCardsComponent } from './restaurants-cards.component';

describe('RestaurantsCardsComponent', () => {
  let component: RestaurantsCardsComponent;
  let fixture: ComponentFixture<RestaurantsCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantsCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantsCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
