import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpEventType} from '@angular/common/http';
import {AppConstants} from '../../../app.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';



@Component({
  selector: 'app-restaurant-profile-editor',
  templateUrl: './restaurant-profile-editor.component.html',
  styleUrls: ['./restaurant-profile-editor.component.scss']
})
export class RestaurantProfileEditorComponent implements OnInit {
  action: string;
  baseUrl: string;
  actionType: string;
  profileForm = new FormGroup({
    name_ar : new FormControl(''),
    name_en: new FormControl(''),
    address_en: new FormControl(''),
    address_ar: new FormControl(''),
    tel: new FormControl(''),
    area: new FormControl(''),
    email: new FormControl(''),
  });
  areaValue: number;
  imageIdValue: number;
  selectedFile: File;
  encodedFile: string;
  resId: number;
  imageValueFromParent: string;
  editMode: boolean;
  id: number;
  areaValueFromParent = {};
  constructor( private client: HttpClient,
    private router: Router , private route: ActivatedRoute, private spinner: NgxSpinnerService) {

       this.action = 'restaurants';
       this.baseUrl = AppConstants.baseURL ;
       this.actionType = 'restaurant';
  }

  ngOnInit() {
    this.editMode = false;
    this.route.paramMap.subscribe(params => {
      if (params.has('id')) {
        this.resId = +params.get('id');
           this.editMode = true ;
        this.getRestaurantProfile();
      }
    });
  }

  sendData() {
    this.spinner.show();

    // TODO: Use EventEmitter with form value

      this.profileForm.value.area = this.areaValue;
      this.profileForm.value['encoded_image'] = this.encodedFile;
      if (this.editMode) {
        this.action = this.action + '/' + this.id;
      }
     const  method = (this.editMode) ? 'PUT' : 'POST';
     const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
     this.client.request(method, this.baseUrl + this.action, {'body' : this.profileForm.value, 'headers': headers}).subscribe(data => {
            alert('Restaurant Has Been Created Successfuly') ;
           this.spinner.hide();
           this.router.navigate(['/restaurants']);
          },
          error  => {
           this.spinner.hide();
           // alert(error[0].message);
          })
  }

  receiveArea($event) {
    this.areaValue = $event ;
  }

  receiveImageId($event) {
    this.encodedFile = $event;
    // this.selectedFile = $event ;
    console.log(this.encodedFile);

  }

  getRestaurantProfile() {
    this.client.get(this.baseUrl + this.actionType + '/details?id=' + this.resId).subscribe(
    data => { this.profileForm.patchValue(data) ;
         this.areaValueFromParent = data['area'];
         this.imageValueFromParent = data['encoded_image'];
         this.id = data['id'];
          this.areaValue = this.areaValueFromParent['id'];
          this.encodedFile = this.imageValueFromParent;
    }
    );
  }



  // upload(uploadData) {
  //   this.client.post(this.baseUrl + this.action, uploadData)
  //     .subscribe(data => {
  //    //   if (event.type === HttpEventType.Response) {

  //         this.imageIdValue = data['id'];
  //         this.profileForm.value['image'] =  this.imageIdValue ;
  //         // console.log(this.imageUploadedId) ;
  //         const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
  //         this.client.post(this.baseUrl + this.action, this.profileForm.value, {headers: headers}).subscribe(data => {
  //           alert('Restaurant Has Been Created Successfuly') ;
  //          console.log('POST Request is successful', data);
  //          this.spinner.hide();
  //          this.router.navigate(['/restaurants']);
  //         },
  //         error  => {
  //          this.spinner.hide();
  //           alert(error[0].message);
  //         console.log('Error', error);
  //         })
  //     // }
  //     });
  // }

}
