import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantProfileEditorComponent } from './restaurant-profile-editor.component';

describe('RestaurantProfileEditorComponent', () => {
  let component: RestaurantProfileEditorComponent;
  let fixture: ComponentFixture<RestaurantProfileEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantProfileEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantProfileEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
