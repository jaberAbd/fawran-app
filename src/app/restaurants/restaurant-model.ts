export interface RestaurantModel {
    id: number;
    name_ar: string;
    name_en: string;
    address_en: string;
    address_ar: string;
    area: number;
    tel: string;
}
