import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RestaurantsListComponent} from './restaurants-list/restaurants-list.component' ;
import {AddNewRestaurantComponent} from './add-new-restaurant/add-new-restaurant.component' ;
const routes: Routes = [
  {
    path: '',
    component: RestaurantsListComponent
  },
  {
    path: '',
    component: AddNewRestaurantComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantsRoutingModule { }
