import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormBuilder, FormGroup, Validators, Form } from '@angular/forms';
// import custom validator to validate that password and confirm password fields match

@Component({
  selector: 'app-add-new-restaurant',
  templateUrl: './add-new-restaurant.component.html',
  styleUrls: ['./add-new-restaurant.component.scss']
})
export class AddNewRestaurantComponent implements OnInit {
  restaurnatForm: FormGroup;

  submitted = false;
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute) { }

  ngOnInit() {

    this.restaurnatForm = this.formBuilder.group({
      name_ar: ['', Validators.required],
      name_en: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      tel: ['', [Validators.required, Validators.minLength(10)]],
      address_en: ['', Validators.required],
      address_ar: ['', Validators.required]
  });
  }

  get f() { return this.restaurnatForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.restaurnatForm.invalid) {
          return;
      }
      alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.restaurnatForm.value))
  }


}
