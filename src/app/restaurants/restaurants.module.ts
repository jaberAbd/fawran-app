import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestaurantsRoutingModule } from './restaurants-routing.module';
import { RestaurantsListComponent } from './restaurants-list/restaurants-list.component';
import { AddNewRestaurantComponent } from './add-new-restaurant/add-new-restaurant.component';
import { RestaurantProfileEditorComponent } from './elements/restaurant-profile-editor/restaurant-profile-editor.component';
import {AreaSelectComponent} from '../area/elements/area-select/area-select.component';
import { RestaurantsTableComponent } from './elements/restaurants-table/restaurants-table.component';
import { RestaurantsCardsComponent } from './elements/restaurants-cards/restaurants-cards.component';
import { FileUploadComponent } from './elements/file-upload/file-upload.component';


@NgModule({
  imports: [
    CommonModule,
    RestaurantsRoutingModule
  ],
  declarations: [RestaurantsListComponent, AddNewRestaurantComponent,
     RestaurantProfileEditorComponent, AreaSelectComponent, RestaurantsTableComponent, RestaurantsCardsComponent, FileUploadComponent]
})
export class RestaurantsModule { }
