import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType} from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-menu-items-list',
  templateUrl: './menu-items-list.component.html',
  styleUrls: ['./menu-items-list.component.scss']
})
export class MenuItemsListComponent implements OnInit {
  resId: number;
  constructor(    private router: Router , private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.has('resId')) {
        this.resId = +params.get('resId');
      }
    });
  }

}
