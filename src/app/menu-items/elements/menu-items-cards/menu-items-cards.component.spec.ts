import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemsCardsComponent } from './menu-items-cards.component';

describe('MenuItemsCardsComponent', () => {
  let component: MenuItemsCardsComponent;
  let fixture: ComponentFixture<MenuItemsCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuItemsCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuItemsCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
