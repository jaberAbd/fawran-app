import { Component, OnInit, Output, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConstants } from '../../../app.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs/Rx';
import { MenuItemModel } from '../../menu-item-model';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-menu-items-cards',
  templateUrl: './menu-items-cards.component.html',
  styleUrls: ['./menu-items-cards.component.scss']
})
export class MenuItemsCardsComponent implements OnInit {

  action: string;
  baseUrl: string;
  @Input() resturantId: number;
  public menu = [];
  constructor(private client: HttpClient, private domSanitizer: DomSanitizer, private spinner: NgxSpinnerService) {

    this.action = 'menu-items';
    this.baseUrl = AppConstants.baseURL;
  }

  ngOnInit() {
    this.spinner.show();
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    this.getMenu().subscribe(data => this.menu = data);

    this.spinner.hide();
  }

  getMenu(): Observable<MenuItemModel[]> {
    return this.client.get<MenuItemModel[]>(this.baseUrl + this.action)
  }

  decode(encodedImage) {
    return this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
    + encodedImage);
  }
}
