import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemProfileEditorComponent } from './menu-item-profile-editor.component';

describe('MenuItemProfileEditorComponent', () => {
  let component: MenuItemProfileEditorComponent;
  let fixture: ComponentFixture<MenuItemProfileEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuItemProfileEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuItemProfileEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
