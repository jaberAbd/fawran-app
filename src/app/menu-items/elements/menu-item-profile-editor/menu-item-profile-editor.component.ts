import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpEventType} from '@angular/common/http';
import {AppConstants} from '../../../app.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';


@Component({
  selector: 'app-menu-item-profile-editor',
  templateUrl: './menu-item-profile-editor.component.html',
  styleUrls: ['./menu-item-profile-editor.component.scss']
})
export class MenuItemProfileEditorComponent implements OnInit {
  action: string;
  baseUrl: string;
  actionType: string;
  editMode: boolean;
  categoryValue: number;
  id: number;
  resId: number;
  imageIdValue: number;
  selectedFile: File;
  encodedFile: string;
  imageValueFromParent: string;
  profileForm = new FormGroup({
    name_ar : new FormControl(''),
    name_en: new FormControl(''),
    description_en: new FormControl(''),
    description_ar: new FormControl(''),
    category: new FormControl(''),
  });
  categoryValueFromParent = {};
  constructor( private client: HttpClient,
    private router: Router , private route: ActivatedRoute, private spinner: NgxSpinnerService) {

       this.action = 'menu-items';
       this.baseUrl = AppConstants.baseURL ;
       this.actionType = 'menu-item';
       this.editMode = false;
  }
  ngOnInit() { /// on init of the page  check if there are paramters in url ativate edit mode
    /// if there are no paramters on url de activate edit mode so we are in add mode
    this.editMode = false;
    this.route.paramMap.subscribe(params => {
      if (params.has('id')) {
        this.id = +params.get('id'); // + sign to convert from string to integer
        this.resId = +params.get('resId');
           this.editMode = true ;
        this.getItemProfile();
      }
    });
  }
  sendData() {
    this.spinner.show();

    // TODO: Use EventEmitter with form value

      // this.profileForm.value.area = this.areaValue;
      // this.profileForm.value['encoded_image'] = this.encodedFile;
      if (this.editMode) {
        this.action = this.action + '/' + this.id;
      }
     const  method = (this.editMode) ? 'PUT' : 'POST';
     this.profileForm.value.category = this.categoryValue;
     this.profileForm.value['encoded_image'] = this.encodedFile;
     const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
     this.client.request(method, this.baseUrl + this.action, {'body' : this.profileForm.value, 'headers': headers}).subscribe(data => {
            alert('New Menu Item Has Been Created Successfuly For This Restaurant') ;
           this.spinner.hide();
           this.router.navigate(['/restaurants']);
          },
          error  => {
           this.spinner.hide();
           // alert(error[0].message);
          })
  }

  getItemProfile() {
    this.client.get(this.baseUrl + this.actionType + '/details?id=' + this.id ).subscribe(
    data => { this.profileForm.patchValue(data) ;
      this.id = data['id'];
      this.imageValueFromParent = data['encoded_image'];
       this.encodedFile = this.imageValueFromParent;
       this.categoryValueFromParent = data['category'];
       this.categoryValue = this.categoryValueFromParent['id'];
    }
    );
  }

/// this  function to get selected file from file upload component
  receiveImageId($event) {
    this.encodedFile = $event;
  }

  receiveCategory($event) {
    this.categoryValue = $event ;
  }
}
