export interface MenuItemModel {
    id: number;
    name_ar: string;
    name_en: string;
    description_en: string;
    description_ar: string;
}
