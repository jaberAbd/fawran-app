import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuItemsRoutingModule } from './menu-items-routing.module';
import { MenuItemsListComponent } from './menu-items-list/menu-items-list.component';
import { AddMenuItemComponent } from './add-menu-item/add-menu-item.component';
import { MenuItemsCardsComponent } from './elements/menu-items-cards/menu-items-cards.component';
import { MenuItemProfileEditorComponent } from './elements/menu-item-profile-editor/menu-item-profile-editor.component';

@NgModule({
  imports: [
    CommonModule,
    MenuItemsRoutingModule
  ],
  declarations: [MenuItemsListComponent, AddMenuItemComponent,
    MenuItemsCardsComponent, MenuItemProfileEditorComponent]
})
export class MenuItemsModule { }
