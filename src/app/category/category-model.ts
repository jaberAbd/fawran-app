export interface CategoryModel {
    id: number;
    name_ar: string;
    name_en: string;
}
