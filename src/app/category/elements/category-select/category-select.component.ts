import {Component, OnInit, Output, Input, EventEmitter, OnChanges, SimpleChange  } from '@angular/core';
import { CategoryModel } from '../../category-model';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConstants} from '../../../app.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-category-select',
  templateUrl: './category-select.component.html',
  styleUrls: ['./category-select.component.scss']
})
export class CategorySelectComponent implements OnChanges, OnInit  {

  categoryValue: number;
  selectedCategory: number;
  action: string;
  baseUrl: string;
  public categories = [];

  categoryGroup = new FormGroup({
    category: new FormControl(''),
  });
  @Output() messageEvent = new EventEmitter<number>();
  @Input() categoryFromParent: object;
  constructor( private client: HttpClient, private spinner: NgxSpinnerService) {

    this.action = 'categories';
    this.baseUrl = AppConstants.baseURL ;
}


  ngOnInit() {
    this.spinner.show();
  const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
  this.getCategoryList().subscribe(data  => this.categories = data) ;
  this.spinner.hide();
  }

  onChange(newValue) {
    console.log(newValue);
    this.messageEvent.emit(newValue)
    // ... do other stuff here ...
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    console.log(SimpleChange);
    const c = changes['categoryFromParent'].currentValue;
    this.categories = [{ 'id': c.id, 'name_ar': c.name_ar, 'name_en': c.name_en }];
    this.selectedCategory = c.id;
  }



  getCategoryList(): Observable<CategoryModel[]> {
    return this.client.get<CategoryModel[]>(this.baseUrl + this.action)
  }

  }


