import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConstants} from '../../../app.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-category-editor',
  templateUrl: './category-editor.component.html',
  styleUrls: ['./category-editor.component.scss']
})
export class CategoryEditorComponent implements OnInit {
  action: string;
  actionType: string;
  baseUrl: string;
  catId: number;
  editMode: boolean;
  profileForm = new FormGroup({
    name_ar : new FormControl(''),
    name_en: new FormControl('')
  });
  id: number;
  categoryValueFromParent = {};
  constructor( private client: HttpClient,
    private router: Router , private route: ActivatedRoute, private spinner: NgxSpinnerService) {
  
      this.action = 'categories';
      this.baseUrl = AppConstants.baseURL ;
      this.actionType = 'category';
  }

  ngOnInit() {
    this.editMode = false;
    this.route.paramMap.subscribe(params => {
      if (params.has('id')) {
        this.catId = +params.get('id');
           this.editMode = true ;
        this.getCategoryProfile();
      }
    });
  }
  onSubmit() {
    this.spinner.show();
  
    // TODO: Use EventEmitter with form value
   //  console.warn(this.profileForm.value);
  
     const  method = (this.editMode) ? 'PUT' : 'POST';
     const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
     if (this.editMode) {
      this.action = this.action + '/' + this.id;
    }
     this.client.request(method, this.baseUrl + this.action , {'body' : this.profileForm.value, 'headers': headers}).subscribe(data => {
            alert('Category Has Been Created Successfuly') ;
           this.spinner.hide();
           this.router.navigate(['/category']);
          },
          error  => {
           this.spinner.hide();
            alert(error[0].message);
          })
    
   
   }
   getCategoryProfile() {
    this.client.get(this.baseUrl + this.action+"/" + this.catId).subscribe(
    data => { this.profileForm.patchValue(data) ;
        
         
         this.id = data['id'];
         
    }
    );
  }
}
