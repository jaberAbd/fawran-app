import { Component, OnInit } from '@angular/core';
import {AppConstants} from '../../../app.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import {Observable} from 'rxjs/Rx';
import {CategoryModel} from '../../category-model';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-category-table',
  templateUrl: './category-table.component.html',
  styleUrls: ['./category-table.component.scss']
})
export class CategoryTableComponent implements OnInit {

  action: string;
  baseUrl: string;
  public categories = [];
  constructor( private client: HttpClient, private spinner: NgxSpinnerService) {

    this.action = 'categories';
    this.baseUrl = AppConstants.baseURL ;
}
  ngOnInit() {
    this.spinner.show();
  const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
  this.getCategories().subscribe(data  => this.categories = data) ;
  this.spinner.hide();

  }
  remove( id : number ){
    console.log(id);
    this.spinner.show();
    this.client.delete(this.baseUrl+this.action+"/"+id).subscribe(data =>{
      this.getCategories().subscribe(data  => this.categories = data) ;
      this.spinner.hide();
    });
  }
  getCategories(): Observable<CategoryModel[]> {
    return this.client.get<CategoryModel[]>(this.baseUrl + this.action)
  }
}
