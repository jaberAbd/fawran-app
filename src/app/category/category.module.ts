import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddCategoryComponent } from './add-category/add-category.component';
import { CategoryEditorComponent } from './elements/category-editor/category-editor.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { CategoryTableComponent } from './elements/category-table/category-table.component';
import { CategorySelectComponent } from './elements/category-select/category-select.component';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AddCategoryComponent, CategoryEditorComponent, CategoryListComponent, CategoryTableComponent, CategorySelectComponent]
})
export class CategoryModule { }
