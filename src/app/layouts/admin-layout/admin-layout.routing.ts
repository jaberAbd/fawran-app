import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { RestaurantsListComponent } from '../../restaurants/restaurants-list/restaurants-list.component';
import { AddNewRestaurantComponent } from '../../restaurants/add-new-restaurant/add-new-restaurant.component';

import { CustomerListComponent } from '../../customers/customer-list/customer-list.component';
import {AddAreaComponent } from '../../area/add-area/add-area.component';
import {AreaListComponent} from '../../area/area-list/area-list.component';

import { MenuItemsListComponent } from '../../menu-items/menu-items-list/menu-items-list.component';
import { AddMenuItemComponent } from '../../menu-items/add-menu-item/add-menu-item.component';
export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'restaurants',    component: RestaurantsListComponent },
    { path: 'restaurants/add',    component: AddNewRestaurantComponent },
    { path: 'restaurants/update/:id',    component: AddNewRestaurantComponent },
    { path: 'area/add',    component: AddAreaComponent },
    { path: 'area',    component: AreaListComponent },
    { path: 'area/update/:id',    component: AddAreaComponent },
    //  --------------- menu routing
    {path: 'restaurants/menu-items/:resId', component: MenuItemsListComponent  },
    {path: 'restaurants/menu-items/add/:resId', component: AddMenuItemComponent },
    {path: 'restaurants/menu-items/update/:resId/:id', component: AddMenuItemComponent },
    // ================== end  of menu routing
    { path: 'customers',    component: CustomerListComponent },
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'restaurants',      component: RestaurantsListComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'table-list',     component: TableListComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent },

];
