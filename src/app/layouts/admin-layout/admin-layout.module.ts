import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { RestaurantsListComponent } from '../../restaurants/restaurants-list/restaurants-list.component';
import { CustomerListComponent } from '../../customers/customer-list/customer-list.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AddAreaComponent } from '../../area/add-area/add-area.component';
import { AreaListComponent } from '../../area/area-list/area-list.component';
import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatCardModule
} from '@angular/material';
import { AddNewRestaurantComponent } from '../../restaurants/add-new-restaurant/add-new-restaurant.component';
import { RestaurantProfileEditorComponent } from '../../restaurants/elements/restaurant-profile-editor/restaurant-profile-editor.component';
import { AreaEditorComponent } from '../../area/elements/area-editor/area-editor.component';
import { AreaTableComponent } from '../../area/elements/area-table/area-table.component';
import { AreaSelectComponent } from '../../area/elements/area-select/area-select.component';
import { RestaurantsTableComponent } from '../../restaurants/elements/restaurants-table/restaurants-table.component';
import { RestaurantsCardsComponent } from '../../restaurants/elements/restaurants-cards/restaurants-cards.component';
import { FileUploadComponent } from '../../restaurants/elements/file-upload/file-upload.component';
import { MenuItemsListComponent } from '../../menu-items/menu-items-list/menu-items-list.component';
import { AddMenuItemComponent } from '../../menu-items/add-menu-item/add-menu-item.component';
import { MenuItemsCardsComponent } from '../../menu-items/elements/menu-items-cards/menu-items-cards.component';
import { MenuItemProfileEditorComponent } from '../../menu-items/elements/menu-item-profile-editor/menu-item-profile-editor.component';
import { AddCategoryComponent } from '../../category/add-category/add-category.component';
import { CategoryEditorComponent } from '../../category/elements/category-editor/category-editor.component';
import { CategoryListComponent } from '../../category/category-list/category-list.component';
import { CategoryTableComponent } from '../../category/elements/category-table/category-table.component';
import { CategorySelectComponent } from '../../category/elements/category-select/category-select.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    HttpClientModule,
    NgxSpinnerModule,
    MatCardModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    RestaurantsListComponent,
    AddNewRestaurantComponent,
    CustomerListComponent,
    RestaurantProfileEditorComponent,
    AddAreaComponent,
    AreaListComponent,
    AreaEditorComponent,
    AreaTableComponent,
    AreaSelectComponent,
    RestaurantsTableComponent,
    RestaurantsCardsComponent,
    FileUploadComponent,
    //  menu
    MenuItemsListComponent,
    AddMenuItemComponent,
    MenuItemsCardsComponent,
    MenuItemProfileEditorComponent,
    /// category
    AddCategoryComponent,
    CategoryEditorComponent,
    CategoryListComponent,
    CategoryTableComponent,
    CategorySelectComponent
  ]
})

export class AdminLayoutModule { }
