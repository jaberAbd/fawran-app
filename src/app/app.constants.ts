import { HttpHeaders } from '@angular/common/http';
export class AppConstants {
    public static get baseURL(): string { return 'http://localhost/fawran/pro/api/'};
    public static get headers(): HttpHeaders {
      return  new HttpHeaders({
            'Content-Type':  'application/json',
          })
    };

}
