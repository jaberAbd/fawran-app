import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AreaRoutingModule } from './area-routing.module';
import { AreaListComponent } from './area-list/area-list.component';
import { AddAreaComponent } from './add-area/add-area.component';
import { AreaEditorComponent } from './elements/area-editor/area-editor.component';
import { AreaTableComponent } from './elements/area-table/area-table.component';
import { AreaSelectComponent } from './elements/area-select/area-select.component';

@NgModule({
  imports: [
    CommonModule,
    AreaRoutingModule
  ],
  declarations: [AreaListComponent, AddAreaComponent, AreaEditorComponent, AreaTableComponent, AreaSelectComponent]
})
export class AreaModule { }
