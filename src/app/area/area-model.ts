export interface AreaModel {
    id: number;
    name_ar: string;
    name_en: string;
}
