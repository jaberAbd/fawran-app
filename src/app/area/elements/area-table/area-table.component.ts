import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConstants} from '../../../app.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import {Observable} from 'rxjs/Rx';
import {AreaModel} from '../../area-model';
@Component({
  selector: 'app-area-table',
  templateUrl: './area-table.component.html',
  styleUrls: ['./area-table.component.scss']
})
export class AreaTableComponent implements OnInit {
  action: string;
  baseUrl: string;
  public areas = [];
  constructor( private client: HttpClient, private spinner: NgxSpinnerService) {

    this.action = 'areas';
    this.baseUrl = AppConstants.baseURL ;
}

  ngOnInit() {
  this.spinner.show();
  const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
  this.getAreas().subscribe(data  => this.areas = data) ;
  this.spinner.hide();

  }

  getAreas(): Observable<AreaModel[]> {
    return this.client.get<AreaModel[]>(this.baseUrl + this.action)
  }
}
