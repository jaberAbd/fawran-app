import { Component, OnInit, Output, Input, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { AreaModel } from '../../area-model';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConstants} from '../../../app.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import {Observable} from 'rxjs/Rx';
@Component({
  selector: 'app-area-select',
  templateUrl: './area-select.component.html',
  styleUrls: ['./area-select.component.scss']
})

export class AreaSelectComponent implements OnChanges, OnInit {
  areaValue: number;
  selectedArea: number;
  action: string;
  baseUrl: string;
  public areas = [];

  areaGroup = new FormGroup({
    area: new FormControl(''),
  });
  @Output() messageEvent = new EventEmitter<number>();
  @Input() areaFromParent: object;
  constructor( private client: HttpClient, private spinner: NgxSpinnerService) {

    this.action = 'areas';
    this.baseUrl = AppConstants.baseURL ;
}


  ngOnInit() {
  this.spinner.show();
  const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
  this.getAreaList().subscribe(data  => this.areas = data) ;
  this.spinner.hide();
  }

  onChange(newValue) {
    console.log(newValue);
    this.messageEvent.emit(newValue)
    // ... do other stuff here ...
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    const c = changes['areaFromParent'].currentValue;
    this.areas = [{ 'id': c.id, 'name_ar': c.name_ar, 'name_en': c.name_en }];
    this.selectedArea = c.id;
  }



  getAreaList(): Observable<AreaModel[]> {
    return this.client.get<AreaModel[]>(this.baseUrl + this.action)
  }

}
