import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConstants} from '../../../app.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-area-editor',
  templateUrl: './area-editor.component.html',
  styleUrls: ['./area-editor.component.scss']
})
export class AreaEditorComponent implements OnInit {
  action: string;
  baseUrl: string;

  profileForm = new FormGroup({
    name_ar : new FormControl(''),
    name_en: new FormControl('')
  });
  constructor( private client: HttpClient, private spinner: NgxSpinnerService,
  private router: Router) {

    this.action = 'areas';
    this.baseUrl = AppConstants.baseURL ;
}

ngOnInit() {
}
onSubmit() {
 this.spinner.show();
 const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
 // TODO: Use EventEmitter with form value
//  console.warn(this.profileForm.value);
this.client.post(this.baseUrl + this.action, this.profileForm.value, {headers: headers}).subscribe(data => {
  alert('Area Has Been Created Successfuly') ;

  this.spinner.hide();
  this.router.navigate(['/area']);
},
error  => {
 this.spinner.hide();
  alert(error[0].message);
console.log('Error', error);
})
}
}
